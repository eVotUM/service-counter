#!/bin/bash

ENV='dev'
SECRET_KEY="`tr -dc "A-Za-z0-9A-Za-z0-9!@#$%&(_=)><" < /dev/urandom | head -c 128`"

MYSQL_HOST='127.0.0.1'
MYSQL_DATABASE='evotecounter'
MYSQL_USER='root'
MYSQL_PASSWORD='123456'

SN_EVOTE_CS='evote-cs.dev.eurotux.com'


usage="$(basename "$0") -- eVote counter service instalation script

Usage: $(basename "$0") [options]

Options:
    --help      show this help text
    --env       insert environment (dev|staging|production)
    --db-host   insert database hostname (default: localhost)
    --db-name   insert database name
    --db-user   insert database user name
    --db-pass   insert database user password
    --sn-cs     insert server name for the counter service
    --sn-as     insert server name for the anonymizer service
    "


while [ "$1" != "" ]; do
    case $1 in
        --help    ) shift
                    echo "$usage"
                    exit 1;
                    ;;
        --env     ) shift
                    ENV=$1
                    ;;
        --db-host ) shift
                    MYSQL_HOST=$1
                    ;;
        --db-name ) shift
                    MYSQL_DATABASE=$1
                    ;;
        --db-user ) shift
                    MYSQL_USER=$1
                    ;;
        --db-pass ) shift
                    MYSQL_PASSWORD=$1
                    ;;
        --sn-cs   ) shift
                    SN_EVOTE_CS=$1
                    ;;
        --sn-as   ) shift
                    SN_EVOTE_AS=$1
                    ;;
        * )         echo "Option not found. Try './install.sh --help' for more information."
                    exit 1
    esac
    shift
done

# Database check
if ! mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD -h $MYSQL_HOST -e "use $MYSQL_DATABASE"; then
    if ! mysql -u $MYSQL_USER --password=$MYSQL_PASSWORD -h $MYSQL_HOST -e "create database $MYSQL_DATABASE"; then
        echo "No database found and no permissions to create new database"
        exit 1
    fi
fi

function escape {
    echo $1 | sed -e 's/\\/\\\\/g; s/\//\\\//g; s/&/\\\&/g'
}

sed -i "s/DJANGO_SETTINGS_MODULE=.*/DJANGO_SETTINGS_MODULE='evotecounter\.settings\.$ENV'/;" /srv/et/evote-counter/config.env
sed -i "s/SECRET_KEY=.*/SECRET_KEY='$(escape $SECRET_KEY)'/;" /srv/et/evote-counter/config.env

sed -i "s/MYSQL_HOST=.*/MYSQL_HOST='$(escape $MYSQL_HOST)'/;" /srv/et/evote-counter/config.env
sed -i "s/MYSQL_DATABASE=.*/MYSQL_DATABASE='$MYSQL_DATABASE'/;" /srv/et/evote-counter/config.env
sed -i "s/MYSQL_USER=.*/MYSQL_USER='$MYSQL_USER'/;" /srv/et/evote-counter/config.env
sed -i "s/MYSQL_PASSWORD=.*/MYSQL_PASSWORD='$MYSQL_PASSWORD'/;" /srv/et/evote-counter/config.env

sed -i "s/SN_EVOTE_AS=.*/SN_EVOTE_AS='$(escape $SN_EVOTE_AS)'/;" /srv/et/evote-counter/config.env
sed -i "s/SN_EVOTE_CS=.*/SN_EVOTE_CS='$(escape $SN_EVOTE_CS)'/;" /srv/et/evote-counter/config.env

sed -i "s/server_name .*/server_name $(escape $SN_EVOTE_CS);/;" /srv/et/evote-counter/scripts/evote.conf

systemctl restart nginx.service
supervisorctl update
supervisorctl restart evote:

cd /srv/et/evote-counter/evotecounter
source /srv/et/evote-counter/config.env
/srv/et/evote-counter/env/bin/python manage.py collectstatic --no-input --settings=$DJANGO_SETTINGS_MODULE
/srv/et/evote-counter/env/bin/python manage.py migrate --no-input --settings=$DJANGO_SETTINGS_MODULE
