%{!?enviroment:%define enviroment production}
%global _python_bytecompile_errors_terminate_build 0
%define  debug_package %{nil}

Name:           evote-counter
Version:        0.6.2
Release:        1%{?dist}
Summary:        counter eVote service

Group:          Applications/Internet
License:        GPLv2+
URL:            http://eurotux.com
Source0:        sources-counter.tgz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: python
BuildRequires: python-pip
BuildRequires: mariadb-devel
BuildRequires: openssl-devel
BuildRequires: libffi-devel
BuildRequires: libjpeg-devel
BuildRequires: libpng-devel
BuildRequires: freetype-devel
BuildRequires: python-virtualenv
BuildRequires: gcc
BuildRequires: gcc-c++
Requires: mariadb
Requires: python-virtualenv
Requires: python-pip
Requires: supervisor
Requires: nginx

AutoReqProv:    no

%description
counter eVote service


%prep
%setup -q -n sources-counter


%build


%install
rm -rf $RPM_BUILD_ROOT
# Create all directories
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-counter/docs
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-counter/data
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-counter/logs
mkdir -p $RPM_BUILD_ROOT/srv/et/evote-counter/run
# Copy code to final path
cp -r evotecounter $RPM_BUILD_ROOT/srv/et/evote-counter/
# Copy sripts to final path
cp -r builds/scripts $RPM_BUILD_ROOT/srv/et/evote-counter/
# Copy config to final path
cp -r builds/config.env $RPM_BUILD_ROOT/srv/et/evote-counter/
# Install all python dependencies with pip
cd $RPM_BUILD_ROOT/srv/et/evote-counter/
virtualenv env
env/bin/pip install pip --upgrade
env/bin/pip install setuptools --upgrade
env/bin/pip install -r evotecounter/evotecounter/requirements/%{enviroment}.txt;
echo "source /srv/et/evote-counter/config.env" >> env/bin/activate
# Remove RPM_BUILD_ROOT string from files
sed -i "s|${RPM_BUILD_ROOT}||g" env/bin/*
# Add script service to nginx (symbolic links)
mkdir -p $RPM_BUILD_ROOT/etc/nginx/conf.d
ln -s /srv/et/evote-counter/scripts/evote.conf $RPM_BUILD_ROOT/etc/nginx/conf.d/evote.conf
# Add script service to supervisor (symbolic links)
mkdir -p $RPM_BUILD_ROOT/etc/supervisord.d
ln -s /srv/et/evote-counter/scripts/supervisor.ini $RPM_BUILD_ROOT/etc/supervisord.d/evote.ini


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
/srv/et/evote-counter/docs
/srv/et/evote-counter/data
/srv/et/evote-counter/logs
/srv/et/evote-counter/run
/srv/et/evote-counter/evotecounter
/srv/et/evote-counter/env
/srv/et/evote-counter/scripts/install.sh
/srv/et/evote-counter/scripts/*_start
%config(noreplace) /srv/et/evote-counter/config.env
%config(noreplace) /srv/et/evote-counter/scripts/evote.conf
%config(noreplace) /srv/et/evote-counter/scripts/supervisor.ini
%config(noreplace) /etc/nginx/conf.d/evote.conf
%config(noreplace) /etc/supervisord.d/evote.ini

%post
# Code for install rpm
if [ "$1" == "1" ]; then
    useradd evote
    chown -R evote:evote /srv/et/evote-counter

    systemctl enable nginx.service
    systemctl start nginx.service

    systemctl enable supervisord.service
    systemctl start supervisord.service
    supervisorctl update
    supervisorctl start evote:

    echo "To finish the instalation use the command '/srv/et/evote-counter/scripts/install.sh'"

# Code for update rpm
else
    chown -R evote:evote /srv/et/evote-counter
    systemctl reload nginx.service
    supervisorctl restart evote:
fi


%changelog

* Sun Mar 19 2017 Sandro Rodrigues <sfr@eurotux.com> 0.6.2-1
- Updated source code

* Fri Mar 10 2017 Sandro Rodrigues <sfr@eurotux.com> 0.6.1-1
- Removed localpkgs dependency;
- Removed directories cache and backup;

* Wed Jan 25 2017 Sandro Rodrigues <sfr@eurotux.com> 0.6-2
- Replaced gracefull reload of gunicorn to restart

* Sun Jan 8 2017 André Rocha <asr@eurotux.com> 0.6-1
- Added unit tests
- Added Ping API endpoint
- Removed parallel from unit tests due to multiprocess forking

* Fri Jan 6 2017 Luis Silva <lms@eurotux.com> 0.5.2-1
- Bug fixed in install.sh
- chown in rpm update
- Updated source code

* Thu Jan 5 2017 Luis Silva <lms@eurotux.com> 0.5.1-3
- Bug fixed in install.sh

* Wed Jan 4 2017 Luis Silva <lms@eurotux.com> 0.5.1-2
- Created new release

* Wed Jan 4 2017 Sandro Rodrigues <sfr@eurotux.com> 0.5.1-1
- Updated source code

* Thu Dec 19 2016 André Rocha <asr@eurotux.com> 0.5-2
- Post-install fixes

* Thu Dec 15 2016 André Rocha <asr@eurotux.com> 0.5-1
- First installable version

* Wed Nov 09 2016 Carlos Rodrigues <cmar@eurotux.com> 0.1-1
- counter eVote service first version
