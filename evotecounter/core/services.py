# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import requests


class AnonymizerService:
    base_url: str
    verify_ssl: bool
    timeout: int
    headers: dict

    def __init__(self, base_url, api_key, verify_ssl=True, timeout=10):
        self.base_url = base_url
        self.verify_ssl = verify_ssl
        self.timeout = timeout
        self.headers = {"content-type": "application/json", "Authorization": f"Api-Key {api_key}"}

    def votes_list(self, election_id, page=1):
        """
        Method to handle vote listing.
        """
        url = f"{self.base_url}/api/v1/elections/{election_id}/votes/"
        response = requests.get(url, {"page": page}, verify=self.verify_ssl, timeout=self.timeout, headers=self.headers)
        response.raise_for_status()
        vote_list = response.json()
        return (vote_list["results"], bool(vote_list["next"]))

    def votes_update(self, election_id, votes):
        """
        Method to send a batch of signed votes to update.
        """
        url = f"{self.base_url}/api/v1/elections/{election_id}/votes/bulk/"
        response = requests.put(url, json=votes, verify=self.verify_ssl, timeout=self.timeout, headers=self.headers)
        response.raise_for_status()
        return True
