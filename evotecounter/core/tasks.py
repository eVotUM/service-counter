# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json
import logging

from django.conf import settings
from django.utils import timezone

from eVotUM.Cripto import eccblind, pkiutils

from evotecounter.celery import app

from .models import ElectionResult
from .services import AnonymizerService
from .utils import fuse_shared_keys, get_private_key_password
from .validators import BallotPaperValidator

logger = logging.getLogger(__name__)


@app.task(ignore_result=True)
def start_election_scrutiny(election_result_id, scrunity_data):
    try:
        election_result: ElectionResult = ElectionResult.objects.get(pk=election_result_id)
        election_result.status = ElectionResult.Status.SIGNATURE
        election_result.save(update_fields=["modified", "status"])

        ballot_paper_validator = BallotPaperValidator(scrunity_data["election_info"])

        password = fuse_shared_keys(
            scrunity_data["shared_secret_keys"],
            scrunity_data["electoral_process_id"],
            scrunity_data["election_management_cert"],
        )

        results = {
            "candidates": {},
            "blank_votes": {},
            "total_votes": 0,
            "invalid_signature": 0,
            "invalid_cipher": 0,
            "invalid_format": 0,
            "invalid_votes": 0,
        }
        anonymizer_service = AnonymizerService(
            settings.ANONYMIZER_API_BASE_URL,
            api_key=settings.ANONYMIZER_API_KEY,
            verify_ssl=settings.ANONYMIZER_API_VERIFY_SSL,
        )
        more = True
        page = 1
        while more:
            batch_votes = []
            (votes, more) = anonymizer_service.votes_list(election_result.election_id, page)

            for vote in votes:
                # Update the flat number of total votes
                results["total_votes"] += 1

                (err, _valid) = eccblind.verifySignature(
                    scrunity_data["validator_cert"],
                    vote["signed_vote_hash"],
                    vote["blind_components"],
                    vote["pr_components"],
                    vote["ciphered_vote"],
                )
                # If vote is invalid send to the invalid votes list and continue
                if err is not None:
                    results["invalid_signature"] += 1
                    logger.debug("Found invalid signature for vote_hash='%s'.", vote["signed_vote_hash"])
                    continue

                (err, deciphered_vote) = pkiutils.decryptWithPrivateKey(
                    vote["ciphered_vote"], scrunity_data["election_private_key"], password
                )
                # If the vote was not properly deciphered send to the invalid votes list and continue
                if err is not None:
                    results["invalid_cipher"] += 1
                    logger.debug("Found invalid cipher for vote_hash='%s'.", vote["signed_vote_hash"])
                    continue

                try:
                    opened_vote = json.loads(deciphered_vote)
                except (ValueError, TypeError):
                    results["invalid_format"] += 1
                    logger.debug("Found json invalid format for vote_hash='%s'.", vote["signed_vote_hash"])
                    continue

                if not ballot_paper_validator.is_valid(opened_vote):
                    results["invalid_format"] += 1
                    logger.debug("Found invalid format for vote_hash='%s'.", vote["signed_vote_hash"])
                    continue

                if len(opened_vote["candidates"]) == 0:
                    if opened_vote["category"] in results["blank_votes"]:
                        results["blank_votes"][opened_vote["category"]] += 1
                    else:
                        results["blank_votes"][opened_vote["category"]] = 1
                else:
                    for candidate in opened_vote["candidates"]:
                        vote_weight = int(candidate.get("vote", 1))

                        if candidate["id"] not in results["candidates"]:
                            results["candidates"].setdefault(candidate["id"], {opened_vote["category"]: vote_weight})
                        else:
                            results["candidates"][candidate["id"]][opened_vote["category"]] += vote_weight

                # Sign the vote and set the timestamp
                (err, signed_vote) = pkiutils.signObject(
                    json.dumps(opened_vote), scrunity_data["counter_private_key"], get_private_key_password()
                )
                if err is not None:
                    raise Exception(f"({err}) Could not sign vote")

                # Set the vote to be updated
                batch_votes.append({
                    "id": vote["id"],
                    "deciphered_vote": signed_vote,
                    "decipher_timestamp": timezone.now().isoformat(),
                })

            # Update this batch of votes
            if batch_votes:
                anonymizer_service.votes_update(election_result.election_id, batch_votes)

            # Process the next batch
            page += 1

        results["invalid_votes"] = results["invalid_format"] + results["invalid_cipher"] + results["invalid_signature"]

        election_result.result = results
        election_result.status = ElectionResult.Status.PUBLISHED
        election_result.save(update_fields=["modified", "result", "status"])
    except Exception as e:
        logger.exception(e)
        election_result.status = ElectionResult.Status.ERROR
        election_result.save(update_fields=["modified", "status"])
