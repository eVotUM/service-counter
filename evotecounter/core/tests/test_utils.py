# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from unittest.mock import patch

from django.test import SimpleTestCase

from core.utils import fuse_shared_keys


class FuseSharedKeysTest(SimpleTestCase):
    @patch("core.utils.shamirsecret.recoverSecretFromComponents")
    def test_fuse_shared_keys_incorrect(self, mock_fn):
        mock_fn.return_value = (1, None)
        with self.assertRaisesMessage(Exception, "(1) Unable to recover shared secret"):
            fuse_shared_keys(["1", "2", "3"], 1, "dummy")

    @patch("core.utils.shamirsecret.recoverSecretFromComponents")
    def test_fuse_shared_keys_success(self, mock_fn):
        secret = "dummysecret"  # NOQA: S105
        mock_fn.return_value = (None, secret)
        shared_secret = fuse_shared_keys(["1", "2", "3"], 1, "dummy")
        self.assertEqual(secret, shared_secret)
