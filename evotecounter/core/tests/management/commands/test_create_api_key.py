# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from io import StringIO

from django.core.management import call_command
from django.test import TestCase

from rest_framework_api_key.models import APIKey


class CreateApiKeyTest(TestCase):
    """ """

    def test_create_api_key(self):
        out = StringIO()
        call_command("create_api_key", "test-api-key", stdout=out)
        self.assertEqual(APIKey.objects.count(), 1)
        self.assertNotIn("API Keys deleted.", out.getvalue())
        self.assertIn("The API key for test-api-key is: ", out.getvalue())
        self.assertIn("Please store it somewhere safe: you will not be able to see it again.", out.getvalue())

    def test_create_api_key_clear_all_option(self):
        out = StringIO()
        APIKey.objects.create_key(name="will be deleted")
        call_command("create_api_key", "test-api-key", clear_all=True, stdout=out)
        self.assertEqual(APIKey.objects.count(), 1)
        self.assertIn("1 API Keys deleted.", out.getvalue())
        self.assertIn("The API key for test-api-key is: ", out.getvalue())
        self.assertIn("Please store it somewhere safe: you will not be able to see it again.", out.getvalue())

    def test_create_api_key_minimal_output(self):
        out = StringIO()
        call_command("create_api_key", "test-api-key", minimal_output=True, stdout=out)
        self.assertEqual(APIKey.objects.count(), 1)
        self.assertIn(APIKey.objects.get(name="test-api-key").prefix, out.getvalue())

    def test_create_api_key_minimal_output_and_clear_all(self):
        out = StringIO()
        APIKey.objects.create_key(name="will be deleted")
        call_command("create_api_key", "test-api-key", minimal_output=True, clear_all=True, stdout=out)
        self.assertEqual(APIKey.objects.count(), 1)
        self.assertNotIn("1 API Keys deleted.", out.getvalue())
        self.assertIn(APIKey.objects.get(name="test-api-key").prefix, out.getvalue())
