# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


class ScrutinyTestUtils:
    def create_received_data(self):
        return {
            "root_cert": "dummy_root",
            "validator_cert": "dummy",
            "counter_private_key": "dummy",
            "election_private_key": "dummy",
            "election_management_cert": "dummy",
            "shared_secret_keys": ["secret1", "secret2", "secret3"],
            "electoral_process_id": 1,
            "election_info": {
                "limit_remarkable_choices": 1,
                "is_ordered_voting": False,
                "valid_candidate_ids": [1, 2, 3],
                "valid_category_slugs": ["one"],
            },
        }

    def create_vote_obj(self, obj_id):
        return {
            "ciphered_vote": "dummycipher",
            "signed_vote_hash": "dummysignature",
            "blind_components": "comp1",
            "pr_components": "comp2",
            "id": obj_id,
        }

    def create_vote(self, blank=False, category="one", candidates=None):
        if candidates is None:
            candidates = [{"id": "1", "vote": 1}]
        return {"blank": blank, "category": category, "candidates": candidates}
