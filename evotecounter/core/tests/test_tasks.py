# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import json
from unittest.mock import Mock, patch

from django.test import TestCase

from core.models import ElectionResult
from core.tasks import start_election_scrutiny

from .utils import ScrutinyTestUtils


@patch("core.tasks.fuse_shared_keys", new=Mock(return_value="dummysecret"))
class StartElectionScrutinyTest(ScrutinyTestUtils, TestCase):
    """ """

    def setUp(self):
        self.election_id = 5
        self.received_data = self.create_received_data()
        # Vote objects from the AS
        self.vote_list = [self.create_vote_obj(i) for i in range(0, 5)]
        self.deciphered_vote = json.dumps(self.create_vote())

    @patch("core.tasks.AnonymizerService")
    def test_invalid_signatures(self, mock_anonymizer_service):
        mock_anonymizer_service.return_value.votes_list.return_value = (self.vote_list, False)
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        start_election_scrutiny(election_result.pk, self.received_data)
        election_result.refresh_from_db()
        self.assertEqual(election_result.status, ElectionResult.Status.PUBLISHED)
        self.assertEqual(election_result.result["total_votes"], len(self.vote_list))
        self.assertEqual(election_result.result["invalid_votes"], len(self.vote_list))
        self.assertEqual(election_result.result["invalid_signature"], len(self.vote_list))

    @patch("core.tasks.eccblind.verifySignature", new=Mock(return_value=(None, True)))
    @patch("core.tasks.AnonymizerService")
    def test_invalid_cipher(self, mock_anonymizer_service):
        mock_anonymizer_service.return_value.votes_list.return_value = (self.vote_list, False)
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        start_election_scrutiny(election_result.pk, self.received_data)
        election_result.refresh_from_db()
        self.assertEqual(election_result.status, ElectionResult.Status.PUBLISHED)
        self.assertEqual(election_result.result["total_votes"], len(self.vote_list))
        self.assertEqual(election_result.result["invalid_votes"], len(self.vote_list))
        self.assertEqual(election_result.result["invalid_cipher"], len(self.vote_list))

    @patch("core.tasks.pkiutils.decryptWithPrivateKey")
    @patch("core.tasks.eccblind.verifySignature", new=Mock(return_value=(None, True)))
    @patch("core.tasks.AnonymizerService")
    def test_invalid_format_deciphered_vote(self, mock_anonymizer_service, mock_decrypt):
        mock_decrypt.return_value = (None, "")
        mock_anonymizer_service.return_value.votes_list.return_value = (self.vote_list, False)
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        start_election_scrutiny(election_result.pk, self.received_data)
        election_result.refresh_from_db()
        self.assertEqual(election_result.status, ElectionResult.Status.PUBLISHED)
        self.assertEqual(election_result.result["total_votes"], len(self.vote_list))
        self.assertEqual(election_result.result["invalid_votes"], len(self.vote_list))
        self.assertEqual(election_result.result["invalid_format"], len(self.vote_list))

    @patch("core.tasks.pkiutils.decryptWithPrivateKey")
    @patch("core.tasks.eccblind.verifySignature", new=Mock(return_value=(None, True)))
    @patch("core.tasks.AnonymizerService")
    def test_sign_object_error(self, mock_anonymizer_service, mock_decrypt):
        mock_decrypt.return_value = (None, self.deciphered_vote)
        mock_anonymizer_service.return_value.votes_list.return_value = (self.vote_list, False)
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        start_election_scrutiny(election_result.pk, self.received_data)
        election_result.refresh_from_db()
        self.assertEqual(election_result.status, ElectionResult.Status.ERROR)

    @patch("core.tasks.pkiutils.signObject", new=Mock(return_value=(None, "signedvote")))
    @patch("core.tasks.pkiutils.decryptWithPrivateKey")
    @patch("core.tasks.eccblind.verifySignature", new=Mock(return_value=(None, True)))
    @patch("core.tasks.AnonymizerService")
    def test_update_votes_error_response(self, mock_anonymizer_service, mock_decrypt):
        mock_decrypt.return_value = (None, self.deciphered_vote)
        mock_anonymizer_service.return_value.votes_list.return_value = (self.vote_list, False)
        mock_anonymizer_service.return_value.votes_update.side_effect = Exception("API Response with 400 status code")
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        start_election_scrutiny(election_result.pk, self.received_data)
        election_result.refresh_from_db()
        self.assertEqual(election_result.status, ElectionResult.Status.ERROR)

    @patch("core.tasks.pkiutils.signObject", new=Mock(return_value=(None, "signedvote")))
    @patch("core.tasks.pkiutils.decryptWithPrivateKey")
    @patch("core.tasks.eccblind.verifySignature", new=Mock(return_value=(None, True)))
    @patch("core.tasks.AnonymizerService")
    def test_success(self, mock_anonymizer_service, mock_decrypt):
        mock_decrypt.return_value = (None, self.deciphered_vote)
        mock_anonymizer_service.return_value.votes_list.return_value = (self.vote_list, False)
        mock_anonymizer_service.return_value.votes_update.return_value = {"invalid": 0, "failed": []}
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        start_election_scrutiny(election_result.pk, self.received_data)
        election_result.refresh_from_db()
        self.assertEqual(election_result.status, ElectionResult.Status.PUBLISHED)
        self.assertDictEqual(
            election_result.result,
            {
                "candidates": {"1": {"one": 5}},
                "blank_votes": {},
                "total_votes": 5,
                "invalid_signature": 0,
                "invalid_cipher": 0,
                "invalid_format": 0,
                "invalid_votes": 0,
            },
        )

    @patch("core.tasks.pkiutils.signObject", new=Mock(return_value=(None, "signedvote")))
    @patch("core.tasks.pkiutils.decryptWithPrivateKey")
    @patch("core.tasks.eccblind.verifySignature", new=Mock(return_value=(None, True)))
    @patch("core.tasks.AnonymizerService")
    def test_success_with_blank_vote(self, mock_anonymizer_service, mock_decrypt):
        mock_decrypt.return_value = (None, json.dumps(self.create_vote(candidates=[])))
        mock_anonymizer_service.return_value.votes_list.return_value = (self.vote_list, False)
        mock_anonymizer_service.return_value.votes_update.return_value = {"invalid": 0, "failed": []}
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        start_election_scrutiny(election_result.pk, self.received_data)
        election_result.refresh_from_db()
        self.assertEqual(election_result.status, ElectionResult.Status.PUBLISHED)
        self.assertDictEqual(
            election_result.result,
            {
                "candidates": {},
                "blank_votes": {"one": 5},
                "total_votes": 5,
                "invalid_signature": 0,
                "invalid_cipher": 0,
                "invalid_format": 0,
                "invalid_votes": 0,
            },
        )
