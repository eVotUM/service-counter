# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.test import SimpleTestCase

from core.validators import BallotPaperValidator


class IsVoteValidTest(SimpleTestCase):
    def setUp(self):
        self.validator = BallotPaperValidator({
            "limit_remarkable_choices": 1,
            "is_ordered_voting": False,
            "valid_candidate_ids": [1, 2, 3],
            "valid_category_slugs": ["one", "two"],
        })

    def test_is_valid_fail(self):
        self.assertFalse(self.validator.is_valid({}))

    def test_is_valid(self):
        candidates = [{"id": 1, "vote": 1}, {"id": 2, "vote": 1}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        self.assertTrue(self.validator.validate_ballot_paper_ordering(vote))

    def test_validate_ballot_paper_format_invalid_instance(self):
        msgerror = "Ballot paper must be dict type"
        with self.assertRaisesRegex(AssertionError, msgerror):
            self.validator.validate_ballot_paper_format([])

    def test_validate_ballot_paper_format_invalid_keys(self):
        msgerror = "Ballot paper has invalid keys"
        with self.assertRaisesRegex(AssertionError, msgerror):
            vote = {"test": None}
            self.validator.validate_ballot_paper_format(vote)

    def test_validate_ballot_paper_format_invalid_candidates_type(self):
        msgerror = "Candidates must be list type"
        with self.assertRaisesRegex(AssertionError, msgerror):
            vote = {"blank": None, "category": None, "candidates": None}
            self.validator.validate_ballot_paper_format(vote)

    def test_validate_ballot_paper_format_invalid_candidate_type(self):
        msgerror = "Candidate must be dict type"
        with self.assertRaisesRegex(AssertionError, msgerror):
            vote = {"blank": None, "category": None, "candidates": ["test"]}
            self.validator.validate_ballot_paper_format(vote)

    def test_validate_ballot_paper_format_invalid_candidate_keys(self):
        msgerror = "Candidate has invalid keys"
        with self.assertRaisesRegex(AssertionError, msgerror):
            vote = {"blank": None, "category": None, "candidates": [{"test": None}]}
            self.validator.validate_ballot_paper_format(vote)

    def test_validate_ballot_paper_format_valid(self):
        vote = {"blank": None, "category": None, "candidates": [{"id": None, "vote": None}]}
        self.assertTrue(self.validator.validate_ballot_paper_format(vote))

    def test_validate_limit_remarkable_choices_invalid(self):
        candidates = [{}, {}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        with self.assertRaisesRegex(AssertionError, "Surpassed limit"):
            self.validator.validate_limit_remarkable_choices(vote)

    def test_validate_limit_remarkable_choices_valid(self):
        candidates = []
        vote = {"blank": None, "category": None, "candidates": candidates}
        self.assertTrue(self.validator.validate_limit_remarkable_choices(vote))

    def test_validate_category_invalid(self):
        vote = {"blank": None, "category": "three", "candidates": None}
        with self.assertRaisesRegex(AssertionError, "Invalid category slug"):
            self.validator.validate_category(vote)

    def test_validate_category_valid(self):
        vote = {"blank": None, "category": "one", "candidates": None}
        self.assertTrue(self.validator.validate_category(vote))

    def test_validate_candidates_unexistent(self):
        msgerror = "Invalid candidate selected"
        candidates = [{"id": 1}, {"id": 4}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        with self.assertRaisesRegex(AssertionError, msgerror):
            self.validator.validate_candidates(vote)

    def test_validate_candidates_repeated(self):
        msgerror = "Selected repeated candidate"
        candidates = [{"id": 1}, {"id": 1}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        with self.assertRaisesRegex(AssertionError, msgerror):
            self.validator.validate_candidates(vote)

    def test_validate_candidates_valid(self):
        candidates = [{"id": 1}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        self.assertTrue(self.validator.validate_candidates(vote))

    def test_validate_ballot_paper_ordering_unordered_voting_invalid(self):
        candidates = [{"vote": 2}, {"vote": 1}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        with self.assertRaisesRegex(AssertionError, "Invalid order"):
            self.validator.validate_ballot_paper_ordering(vote)

    def test_validate_ballot_paper_ordering_unordered_voting_valid(self):
        candidates = [{"vote": 1}, {"vote": 1}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        self.assertTrue(self.validator.validate_ballot_paper_ordering(vote))

    def test_validate_ballot_paper_ordering_with_ordered_voting_invalid(self):
        self.validator.election_info["is_ordered_voting"] = True
        self.validator.election_info["limit_remarkable_choices"] = 4
        candidates = [{"vote": 20}, {"vote": 10}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        with self.assertRaisesRegex(AssertionError, "Invalid order"):
            self.validator.validate_ballot_paper_ordering(vote)

    def test_validate_ballot_paper_ordering_ordered_voting_valid(self):
        self.validator.election_info["is_ordered_voting"] = True
        self.validator.election_info["limit_remarkable_choices"] = 4
        candidates = [{"vote": 4}, {"vote": 3}]
        vote = {"blank": None, "category": None, "candidates": candidates}
        self.assertTrue(self.validator.validate_ballot_paper_ordering(vote))
