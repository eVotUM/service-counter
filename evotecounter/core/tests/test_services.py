# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from unittest.mock import patch

from django.test import SimpleTestCase

from core.services import AnonymizerService


class AnonymizerServiceTestCase(SimpleTestCase):
    def setUp(self):
        self.base_url = "http://example.com"
        self.service = AnonymizerService(self.base_url, api_key="key")

    @patch("core.services.requests.get")
    def test_votes_list(self, mock_get):
        # Mock the response from requests.get
        mock_get.return_value.json.return_value = {
            "results": ["vote1", "vote2"],
            "next": "/api/v1/elections/1/votes/?page=2",
        }
        mock_get.return_value.status_code = 200

        results, as_next_page = self.service.votes_list(1, page=1)

        # Assert the expected values
        self.assertEqual(results, ["vote1", "vote2"])
        self.assertTrue(as_next_page)
        mock_get.assert_called_once_with(
            f"{self.base_url}/api/v1/elections/1/votes/",
            {"page": 1},
            verify=True,
            timeout=10,
            headers={"content-type": "application/json", "Authorization": "Api-Key key"},
        )

    @patch("core.services.requests.put")
    def test_votes_update(self, mock_put):
        votes = ["vote1", "vote2"]

        # Mock the response from requests.post
        mock_put.return_value.status_code = 204

        response = self.service.votes_update(1, votes)

        # Assert the expected values
        self.assertTrue(response)
        mock_put.assert_called_once_with(
            f"{self.base_url}/api/v1/elections/1/votes/bulk/",
            json=votes,
            headers={"content-type": "application/json", "Authorization": "Api-Key key"},
            verify=True,
            timeout=10,
        )
