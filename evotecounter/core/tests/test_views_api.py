# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import json

from django.urls import reverse_lazy

from rest_framework.test import APITestCase
from rest_framework_api_key.models import APIKey

from core.models import ElectionResult

from .utils import ScrutinyTestUtils


class ElectionResultRetriveDestroyViewTest(APITestCase):
    """ """

    def setUp(self):
        self.election_id = 5
        _, key = APIKey.objects.create_key(name="test_key")
        self.url = reverse_lazy("api:election_result_retrive_destroy", kwargs={"election_id": self.election_id})
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_destroy_inexistent(self):
        response = self.client.delete(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 404)

    def test_destroy_without_api_key(self):
        ElectionResult.objects.create(election_id=self.election_id)
        self.assertEqual(ElectionResult.objects.count(), 1)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, 403)

    def test_destroy_ok(self):
        ElectionResult.objects.create(election_id=self.election_id)
        self.assertEqual(ElectionResult.objects.count(), 1)
        response = self.client.delete(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(ElectionResult.objects.count(), 0)

    def test_get_election_result_not_found(self):
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 404)

    def test_get_election_without_api_key(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)

    def test_get_election_result_status(self):
        election_result = ElectionResult.objects.create(election_id=self.election_id)
        response = self.client.get(self.url, headers=self.headers)
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.content)
        self.assertEqual(
            json_response,
            {
                "id": election_result.pk,
                "election_id": self.election_id,
                "result": {},
                "status": ElectionResult.Status.EXTRACTION,
            },
        )


class ElectionResultCreateViewTest(ScrutinyTestUtils, APITestCase):
    """ """

    def setUp(self):
        self.election_id = 5
        _, key = APIKey.objects.create_key(name="test_key")
        self.url = reverse_lazy("api:election_result_create")
        self.headers = {"Authorization": f"Api-Key {key}"}

    def test_create_election_result_with_already_existent(self):
        ElectionResult.objects.create(election_id=self.election_id)
        response = self.client.post(
            self.url, {"election_id": self.election_id, **self.create_received_data()}, headers=self.headers
        )
        self.assertEqual(response.status_code, 400)
        self.assertIn("election_id", response.data)
        self.assertEqual(response.data["election_id"][0], "Election Result with this election id already exists.")

    def test_create_election_result_without_api_key(self):
        ElectionResult.objects.create(election_id=self.election_id)
        response = self.client.post(self.url, {"election_id": self.election_id, **self.create_received_data()})
        self.assertEqual(response.status_code, 403)
