# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Generated by Django 5.1.3 on 2024-11-13 17:55

import json

from django.db import migrations, models


def textfield_to_jsonfield(apps, schema_editor):
    ElectionResult = apps.get_model("countercore", "ElectionResult")
    for obj in ElectionResult.objects.all():
        if not obj.election_results:
            obj.result = {}
            obj.save()
        elif isinstance(obj.election_results, str):
            obj.result = json.loads(obj.election_results)
            obj.save()


class Migration(migrations.Migration):
    dependencies = [("countercore", "0002_electionresults_worker_pid")]

    operations = [
        migrations.RenameModel("ElectionResults", "ElectionResult"),
        migrations.RenameField("ElectionResult", "result_status_choices", "status"),
        migrations.AlterModelOptions(
            name="electionresult",
            options={"verbose_name": "Election Result", "verbose_name_plural": "Elections Results"},
        ),
        migrations.RemoveField(model_name="electionresult", name="worker_pid"),
        migrations.AlterField(
            model_name="electionresult",
            name="election_id",
            field=models.PositiveIntegerField(db_index=True, unique=True, verbose_name="election id"),
        ),
        migrations.AlterField(
            model_name="electionresult",
            name="id",
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID"),
        ),
        migrations.AddField(
            model_name="electionresult",
            name="result",
            field=models.JSONField(default=dict, verbose_name="Election results in JSON format"),
        ),
        migrations.RunPython(textfield_to_jsonfield, migrations.RunPython.noop),
        migrations.RemoveField(model_name="electionresult", name="election_results"),
        migrations.AlterField(
            model_name="electionresult",
            name="status",
            field=models.PositiveSmallIntegerField(
                choices=[
                    (0, "Error generating results"),
                    (1, "Extraction"),
                    (2, "Decypher and signature"),
                    (3, "Count"),
                    (4, "Final verification"),
                    (5, "Results published"),
                ],
                default=1,
                verbose_name="Current result status",
            ),
        ),
    ]
