# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand

from rest_framework_api_key.models import APIKey


class Command(BaseCommand):
    help = "Command to generate API Key to authenticate API calls."

    def add_arguments(self, parser):
        parser.add_argument("name", help="Name to identify the key. Don't need to be unique.")
        parser.add_argument("--clear-all", action="store_true", help="Clear all API Key before create a new one.")
        parser.add_argument("--minimal-output", action="store_true", help="Only print API Key.")

    def handle(self, *args, **options):
        if options["clear_all"]:
            count, _ = APIKey.objects.all().delete()
            if not options["minimal_output"] and count:
                self.stdout.write(f"{count} API Keys deleted.\n", self.style.SUCCESS)

        instance, key = APIKey.objects.create_key(name=options["name"])
        if options["minimal_output"]:
            self.stdout.write(key, self.style.SUCCESS)
        else:
            self.stdout.write(f"The API key for {instance.name} is: {key}", self.style.SUCCESS)
            self.stdout.write(
                "Please store it somewhere safe: you will not be able to see it again.", self.style.WARNING
            )
