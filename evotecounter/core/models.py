# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.db import models
from django.db.models.enums import IntegerChoices

from django_extensions.db.models import TimeStampedModel


class ElectionResult(TimeStampedModel):
    """ """

    class Status(IntegerChoices):
        ERROR = 0, "Error generating results"
        EXTRACTION = 1, "Extraction"
        SIGNATURE = 2, "Decypher and signature"
        COUNT = 3, "Count"
        VERIFICATION = 4, "Final verification"
        PUBLISHED = 5, "Results published"

    election_id = models.PositiveIntegerField("election id", db_index=True, unique=True)
    status = models.PositiveSmallIntegerField(
        "Current result status", choices=Status.choices, default=Status.EXTRACTION
    )
    result = models.JSONField("Election results in JSON format", default=dict)

    class Meta:
        verbose_name = "Election Result"
        verbose_name_plural = "Elections Results"

    def __str__(self):
        return f"[{self.get_status_display()}] {self.election_id}"
