# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


import logging

logger = logging.getLogger(__name__)


class BallotPaperValidator:
    def __init__(self, election_info):
        self.election_info = election_info

    def is_valid(self, ballot_paper):
        try:
            self.validate_ballot_paper_format(ballot_paper)
            self.validate_limit_remarkable_choices(ballot_paper)
            self.validate_category(ballot_paper)
            self.validate_candidates(ballot_paper)
            self.validate_ballot_paper_ordering(ballot_paper)
        except AssertionError as e:
            logger.debug("Ballot paper with invalid format: %s", str(e))
            return False
        return True

    def validate_ballot_paper_format(self, ballot_paper):
        """Validate if vote has the expected format"""
        assert isinstance(ballot_paper, dict), "Ballot paper must be dict type"
        expected_keys = ["blank", "category", "candidates"]
        assert set(expected_keys) == set(ballot_paper.keys()), "Ballot paper has invalid keys"
        # validate candidates format
        candidates = ballot_paper.get("candidates")
        assert isinstance(candidates, list), "Candidates must be list type"
        expected_keys = ["id", "vote"]
        for candidate in candidates:
            assert isinstance(candidate, dict), "Candidate must be dict type"
            assert set(expected_keys) == set(candidate.keys()), "Candidate has invalid keys"
        return True

    def validate_limit_remarkable_choices(self, ballot_paper):
        limit = self.election_info.get("limit_remarkable_choices")
        assert len(ballot_paper.get("candidates")) <= limit, "Surpassed limit"
        return True

    def validate_category(self, ballot_paper):
        valid_category_slugs = self.election_info.get("valid_category_slugs")
        assert ballot_paper.get("category") in valid_category_slugs, "Invalid category slug"
        return True

    def validate_candidates(self, ballot_paper):
        valid_candidate_ids = self.election_info.get("valid_candidate_ids")
        selected_candidate_ids = [int(c["id"]) for c in ballot_paper.get("candidates")]
        assert not set(selected_candidate_ids) - set(valid_candidate_ids), "Invalid candidate selected"
        assert len(selected_candidate_ids) == len(set(selected_candidate_ids)), "Selected repeated candidate"
        return True

    def validate_ballot_paper_ordering(self, ballot_paper):
        vote_orders = [c.get("vote") for c in ballot_paper.get("candidates")]
        if not self.election_info.get("is_ordered_voting"):
            # no order, must be all equal to 1, otherwise is invalid
            assert len(vote_orders) == sum(vote_orders), "Invalid order"
        else:
            # order must descrease, starting on top limit
            limit = self.election_info.get("limit_remarkable_choices")
            expected = [limit - i for i in range(0, len(vote_orders))]
            assert vote_orders == expected, "Invalid order"
        return True
