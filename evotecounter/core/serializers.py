# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.db import transaction

from eVotUM.Cripto import pkiutils
from rest_framework import serializers

from core.tasks import start_election_scrutiny

from .models import ElectionResult


class ElectionResultSerializer(serializers.ModelSerializer):
    """ """

    class Meta:
        model = ElectionResult
        fields = ("id", "election_id", "status", "result")


class ElectionInfoSerializer(serializers.Serializer):
    """ """

    limit_remarkable_choices = serializers.IntegerField(help_text="Limit of remarkable choices")
    is_ordered_voting = serializers.BooleanField(help_text="Can choose voting order")
    valid_candidate_ids = serializers.ListField(
        child=serializers.IntegerField(), help_text="List of valid candidates ids"
    )
    valid_category_slugs = serializers.ListField(
        child=serializers.CharField(), help_text="List of valid category slugs"
    )


class ElectionResultCreateSerializer(serializers.ModelSerializer):
    """ """

    root_cert = serializers.CharField(help_text="Root certificate in PEM format", write_only=True)
    validator_cert = serializers.CharField(help_text="Validator Service certificate in PEM format", write_only=True)
    counter_private_key = serializers.CharField(help_text="Counter Service private key in PEM format", write_only=True)
    election_private_key = serializers.CharField(
        help_text="Election certificate private key in PEM format", write_only=True
    )
    election_management_cert = serializers.CharField(
        help_text="Election Management System certificate in PEM format", write_only=True
    )
    shared_secret_keys = serializers.ListField(
        child=serializers.CharField(), help_text="List of shared secret keys", write_only=True
    )
    electoral_process_id = serializers.IntegerField(help_text="Electoral process ID", write_only=True)
    election_info = ElectionInfoSerializer(write_only=True)

    class Meta:
        model = ElectionResult
        fields = (
            "id",
            "election_id",
            "status",
            "root_cert",
            "validator_cert",
            "counter_private_key",
            "election_private_key",
            "election_management_cert",
            "shared_secret_keys",
            "electoral_process_id",
            "election_info",
        )
        read_only_fields = ["id", "status"]

    def validate_root_cert(self, value):
        (error_code, _pub_key) = pkiutils.getPublicKeyFromCertificate(value)
        if error_code is not None:
            raise serializers.ValidationError(f"[{error_code}] Invalid Root certificate")
        return value

    def validate_validator_cert(self, value):
        (error_code, _pub_key) = pkiutils.getPublicKeyFromCertificate(value)
        if error_code is not None:
            raise serializers.ValidationError(f"[{error_code}] Invalid VS certificate")
        return value

    def validate(self, attrs):
        if not pkiutils.verifyCertificateChain(attrs["validator_cert"], attrs["root_cert"]):
            raise serializers.ValidationError("VS certificate unchained")
        return attrs

    def create(self, validated_data):
        instance = ElectionResult.objects.create(election_id=validated_data["election_id"])
        transaction.on_commit(start_election_scrutiny.si(instance.pk, validated_data).delay)
        return instance
