# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.conf import settings

from eVotUM.Cripto import shamirsecret


def get_private_key_password():
    """Get the private key from django."""
    # TODO: Change this to allow defining a differente secret key per service.
    return settings.SECRET_KEY


def fuse_shared_keys(component_list, uid, certificate):
    """
    Combine all secret key components to generate the password.
    """
    for idx, comp in enumerate(component_list):
        component_list[idx] = str(comp)

    error_code, password = shamirsecret.recoverSecretFromComponents(component_list, str(uid), certificate)

    if error_code is not None:
        raise Exception(f"({error_code}) Unable to recover shared secret")

    return password
