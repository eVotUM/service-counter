# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.


from django.urls import path

from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView

from .views_api import ElectionResultCreateView, ElectionResultRetriveDestroyView

app_name = "api"

urlpatterns = [
    path(route="", view=SpectacularSwaggerView.as_view(url_name="api:openapi_schema"), name="swagger_ui"),
    path(route="schema/", view=SpectacularAPIView.as_view(), name="openapi_schema"),
    path(route="scrutiny/", view=ElectionResultCreateView.as_view(), name="election_result_create"),
    path(
        route="scrutiny/<int:election_id>/",
        view=ElectionResultRetriveDestroyView.as_view(),
        name="election_result_retrive_destroy",
    ),
]
