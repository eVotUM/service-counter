# eVotUM - Electronic Voting System
# Copyright (c) 2020 Universidade do Minho
# Developed by Eurotux (dev@eurotux.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from decouple import config
from get_docker_secret import get_docker_secret

# CELERY - http://docs.celeryproject.org/en/latest/userguide/configuration.html

CELERY_BROKER_URL = get_docker_secret("DJANGO_BROKER_URL", default="memory:///")
CELERY_BROKER_USE_SSL = config("DJANGO_BROKER_USE_SSL", default=False, cast=bool)
CELERY_TASK_COMPRESSION = "gzip"
CELERY_TASK_IGNORE_RESULT = True
CELERY_TASK_TIME_LIMIT = 5 * 60 * 60  # five hours
CELERY_WORKER_MAX_MEMORY_PER_CHILD = 500 * 1000  # 500Mb
CELERY_WORKER_HIJACK_ROOT_LOGGER = False
CELERY_TASK_DEFAULT_QUEUE = "counter"
