#!/usr/bin/env python
import os
import sys
from pathlib import Path

import django
from django.conf import settings
from django.test.utils import get_runner

if __name__ == "__main__":
    project_dir = str(Path(__file__).absolute().parent.joinpath("evotecounter"))

    if project_dir not in sys.path:
        sys.path.append(project_dir)

    os.environ["PYTHONWARNINGS"] = "ignore"

    os.environ["DJANGO_SETTINGS_MODULE"] = "evotecounter.settings.test"
    os.environ["DJANGO_SECRET_KEY"] = "secretkey"  # NOQA: S105
    os.environ["ANONYMIZER_API_BASE_URL"] = "https://dummy"

    django.setup()
    TestRunner = get_runner(settings)
    test_runner = TestRunner()
    failures = test_runner.run_tests(sys.argv[1:] or ["evotecounter"])
    sys.exit(bool(failures))
